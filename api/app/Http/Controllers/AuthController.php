<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use KenKoKa\LaraJWT\Facades\JWTAuth;
use App\Core\Exceptions\UnauthorizeException;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $credentials = $request->input();

        if (!\Auth::attempt($credentials)) {
            throw new UnauthorizeException('Login failed. Please recheck the email and password and try again.');
        }

        $loggedInUser = request()->user();

        $accessToken = null;

        try {
            // create access token
            $accessToken = \JWTAuth::attempt($credentials);
        } catch (\JWTAuthException $ex) {
            throw new Exception('Failed to create acess token', 1001);
        }

        return $this->response->array([
            'message' => 'Login successful.',
            'loggedInUserInfo' => $loggedInUser
        ])
        ->withCookie(cookie('token', $accessToken, config('jwt.ttl') , '/', config('session.domain')))
        ->withCookie(cookie('loggedInState', 'logged_in', config('jwt.ttl'), '/', config('session.domain'), false, false));
    }
}
