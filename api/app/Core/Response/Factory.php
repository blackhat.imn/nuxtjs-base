<?php

namespace App\Core\Response;

use Closure;
use ErrorException;
use Illuminate\Support\Str;
use Illuminate\Support\Collection;
use Illuminate\Contracts\Pagination\Paginator;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpFoundation\Response as StatusCode;

use App\Core\Response\Response;
use App\Core\Transformer\Factory as TransformerFactory;

class Factory 
{
    /**
     * Transformer factory instance.
     *
     * @var App\Core\Transfomer\Factory
     */
    protected $transformer;

    /**
     * Create a new response factory instance.
     *
     * @param App\Core\Transformer\Factory $transformer
     *
     * @return void
     */
    public function __construct(TransformerFactory $transformer)
    {
        $this->transformer = $transformer;
    }

    /**
     * Respond with a created response and associate a location if provided.
     *
     * @param null|string $content
     * @param null|string $location
     * @return \App\Core\Response\Response
     */
    public function created($content = null, $location = null)
    {
        $response = new Response($content);
        $response->setStatusCode(StatusCode::HTTP_CREATED);

        if (!is_null($location)) {
            $response->header('Location', $location);
        }

        return $response;
    }

    /**
     * Respond with a no content response
     *
     * @return \App\Core\Response\Response
     */
    public function noContent()
    {
        return (new Response(null))->setStatusCode(StatusCode::HTTP_NO_CONTENT);
    }

    /**
     * Bind a collection to a transformer and start building a response
     *
     * @param Collection $collection
     * @param object $transformer
     * @param array $parameter
     * @param Closure|null $after
     * @return \App\Core\Response\Response
     */
    public function collection(Collection $collection, $transformer, $parameters = [], Closure $after = null)
    {
        if ($collection->isEmpty()) {
            $class = get_class($collection);
        } else {
            $class = get_class($collection->first());
        }

        if ($parameters instanceof \Closure) {
            $after = $parameters;
            $parameters = [];
        }

        $binding = $this->transformer->register($class, $transformer, $parameters, $after);

        return (new Response($collection, StatusCode::HTTP_OK, [], $binding))->make();
    }

    /**
     * Bind an item to a transformer and start building a response.
     *
     * @param object   $item
     * @param object   $transformer
     * @param array    $parameters
     * @param \Closure $after
     *
     * @return \App\Core\Response\Response
     */
    public function item($item, $transformer, $parameters = [], Closure $after = null)
    {
        $class = get_class($item);

        if ($parameters instanceof \Closure) {
            $after = $parameters;
            $parameters = [];
        }

        $binding = $this->transformer->register($class, $transformer, $parameters, $after);

        return (new Response($item, StatusCode::HTTP_OK, [], $binding))->make();
    }

    /**
     * Bind a paginator to a transformer and start building a response.
     *
     * @param \Illuminate\Contracts\Pagination\Paginator $paginator
     * @param object                                     $transformer
     * @param array                                      $parameters
     * @param \Closure                                   $after
     *
     * @return \App\Core\Response\Response
     */
    public function paginator(Paginator $paginator, $transformer, array $parameters = [], Closure $after = null)
    {
        if ($paginator->isEmpty()) {
            $class = get_class($paginator);
        } else {
            $class = get_class($paginator->first());
        }

        $binding = $this->transformer->register($class, $transformer, $parameters, $after);

        return (new Response($paginator, StatusCode::HTTP_OK, [], $binding))->make();
    }

    /**
     * Return an error response.
     *
     * @param string $message
     * @param int    $statusCode
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     *
     * @return void
     */
    public function error($message, $statusCode)
    {
        throw new HttpException($statusCode, $message);
    }

    /**
     * Return a 404 not found error.
     *
     * @param string $message
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     *
     * @return void
     */
    public function errorNotFound($message = 'Not Found')
    {
        $this->error($message, StatusCode::HTTP_NOT_FOUND);
    }

    /**
     * Return a 400 bad request error.
     *
     * @param string $message
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     *
     * @return void
     */
    public function errorBadRequest($message = 'Bad Request')
    {
        $this->error($message, StatusCode::HTTP_BAD_REQUEST);
    }

    /**
     * Return a 403 forbidden error.
     *
     * @param string $message
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     *
     * @return void
     */
    public function errorForbidden($message = 'Forbidden')
    {
        $this->error($message, StatusCode::HTTP_FORBIDDEN);
    }

    /**
     * Return a 500 internal server error.
     *
     * @param string $message
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     *
     * @return void
     */
    public function errorInternal($message = 'Internal Error')
    {
        $this->error($message, StatusCode::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * Return a 401 unauthorized error.
     *
     * @param string $message
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     *
     * @return void
     */
    public function errorUnauthorized($message = 'Unauthorized')
    {
        $this->error($message, StatusCode::HTTP_UNAUTHORIZED);
    }

    /**
     * Return a 405 method not allowed error.
     *
     * @param string $message
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     *
     * @return void
     */
    public function errorMethodNotAllowed($message = 'Method Not Allowed')
    {
        $this->error($message, StatusCode::HTTP_METHOD_NOT_ALLOWED);
    }

    /**
     * Call magic methods beginning with "with".
     *
     * @param string $method
     * @param array  $parameters
     *
     * @throws \ErrorException
     *
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        if (Str::startsWith($method, 'with')) {
            return call_user_func_array([$this, Str::camel(substr($method, 4))], $parameters);

        // Because PHP won't let us name the method "array" we'll simply watch for it
            // in here and return the new binding. Gross. This is now DEPRECATED and
            // should not be used. Just return an array or a new response instance.
        } elseif ($method == 'array') {
            return new Response($parameters[0]);
        }

        throw new ErrorException('Undefined method '.get_class($this).'::'.$method);
    }
}