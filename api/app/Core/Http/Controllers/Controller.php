<?php

namespace App\Core\Http\Controllers;

use App\Core\Http\Traits\Response;

class Controller
{
    use Response;
}