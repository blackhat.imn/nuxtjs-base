<?php

namespace App\Core\Http\Traits;

use Exception;
use App\Core\Response\Factory;

trait Response 
{
    /**
     * Get the response factory instance.
     *
     * @return App\Core\Response\Factory
     */
    public function response()
    {
        return app(Factory::class);
    }

    /**
     * Magically handle calls to certain properties.
     *
     * @param string $key
     * @throws \Exception
     * @return mixed
     */
    public function __get($key)
    {
        $callable = [
            'response',
        ];

        if (in_array($key, $callable) && method_exists($this, $key)) {
            return $this->$key();
        }

        throw new Exception('Undefined property '.get_class($this).'::'.$key);
    }

    /**
     * Magically handle calls to certain methods on the response factory.
     *
     * @param string $method
     * @param array  $parameters
     *
     * @throws Exception
     *
     * @return App\Core\Response\Response
     */
    public function __call($method, $parameters)
    {
        if (method_exists($this->response(), $method) || $method == 'array') {
            return call_user_func_array([$this->response(), $method], $parameters);
        }

        throw new Exception('Undefined method '.get_class($this).'::'.$method);
    }
}