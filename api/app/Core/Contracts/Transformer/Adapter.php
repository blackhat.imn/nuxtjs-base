<?php

namespace App\Core\Contracts\Transformer;

use Illuminate\Http\Request;
use App\Core\Transformer\Binding;

interface Adapter
{
    /**
     * Transform a response with a transformer.
     *
     * @param mixed                          $response
     * @param object                         $transformer
     * @param \App\Core\Transformer\Binding $binding
     * @param Illuminate\Http\Request      $request
     *
     * @return array
     */
    public function transform($response, $transformer, Binding $binding, Request $request);
}
