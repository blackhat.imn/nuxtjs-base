<?php

namespace App\Core\Providers;

use App\Core\Response\Response;

use Illuminate\Support\ServiceProvider;
use App\Core\Transformer\Factory as TransformerFactory;

class CoreServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerClassAliases();
        $this->registerTransformer();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(\App\Core\Contracts\Transformer\Adapter::class, \App\Core\Transformer\Adapter\Fractal::class);
        $this->setResponseStaticInstances();
    }

    /**
     * Register the class aliases.
     *
     * @return void
     */
    protected function registerClassAliases()
    {
        $aliases = [
            'core.transformer' => \App\Core\Transformer\Factory::class,
        ];

        foreach ($aliases as $key => $aliases) {
            foreach ((array) $aliases as $alias) {
                $this->app->alias($key, $alias);
            }
        }
    }

    /**
     * Register the transformer factory.
     *
     * @return void
     */
    protected function registerTransformer()
    {
        $this->app->singleton('core.transformer', function ($app) {
            return new TransformerFactory($app, app()->make(\App\Core\Transformer\Adapter\Fractal::class));
        });
    }

    protected function setResponseStaticInstances()
    {
        Response::setFormatters([
            'json' => app()->make(\App\Core\Response\Format\Json::class)
        ]);

        Response::setFormatsOptions([
            'json' => [
                'pretty_print' => env('API_JSON_FORMAT_PRETTY_PRINT_ENABLED', false),
                'indent_style' => env('API_JSON_FORMAT_INDENT_STYLE', 'space'),
                'indent_size' => env('API_JSON_FORMAT_INDENT_SIZE', 2),
            ],
        ]);

        Response::setTransformer($this->app['core.transformer']);
    }
}