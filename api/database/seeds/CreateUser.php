<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class CreateUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'kencoca',
            'email' => 'nam-le@dimage.co.jp',
            'email_verified_at' => now(),
            'password' => \Hash::make('123456789')
        ]);
    }
}
