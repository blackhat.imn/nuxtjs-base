export default {
    login: 'auth/login',
    logout: 'auth/logout',
    register: 'auth/register',
}