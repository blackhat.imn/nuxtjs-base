import { HTTP_STATUS } from '@/config/consts'

export default function ({
  $axios,
  redirect,
  app
}, inject) {
  let logger = app.$logger;
  // Create a custom axios instance
  const api = $axios.create({
    headers: {
      common: {
        Accept: 'application/json, */*'
      }
    },
    baseURL: process.env.BASE_API_URL || 'http://api.example.test',
    withCredentials: true // not use in example.
  })

  // handle the response from api
  const handleResponse = response => {
    if (process.env.DEBUG) {
      logger.logResponseSuccess(JSON.stringify(response.data));
    }

    return response;
  };

  // handle the error when call api
  const handleError = error => {
    if (!error.response) {
      logger.logResponseError(error)
      return error;
    }

    let errorResponse = Object.assign({}, error.response.data);

    let message = "";
    if (error.response && error.response.data) {
      if (errorResponse.errors) {
        const messages = [];

        Object.keys(errorResponse.errors).forEach(key => {
          messages.push(...errorResponse.errors[key]);
        });

        let ul = document.createElement("ul");
        ul.classList.add("error");
        messages.forEach(m => {
          let li = document.createElement("li");
          li.innerHTML = m;
          ul.appendChild(li);
        });

        message = ul.outerHTML;

        ul.remove();
      } else if (errorResponse.message) {
        message = errorResponse.message;
      }
    }

    let detail = errorResponse.errors;
    let statusCode = errorResponse.status_code;

    // print error when env is not production
    if (process.env.DEBUG) {
      logger.logResponseError(errorResponse);
    }

    try {
      if (
        statusCode == HTTP_STATUS.UNAUTHORIZED &&
        app.router.app._route.name != "auth-login"
      ) {
        // remove all global data in storage
        localStorage.removeItem('user');
        redirect('/login')
      }
    } catch (ex) {
      if (process.env.DEBUG) {
        logger.error(ex);
      }
    }

    return {
      message: message,
      detail: detail,
      status_code: statusCode
    };
  };

  api.interceptors.response.use(
    response => {
      return Promise.resolve(handleResponse(response));
    },
    function (error) {
      return Promise.reject(handleError(error));
    }
  );

  api.interceptors.request.use(
    config => {
      if (process.env.DEBUG) {
        logger.logRequest(config);
      }
      return config;
    },
    error => {
      return handleError(error);
    }
  );

  // Inject to context as $api
  inject('api', api)
}