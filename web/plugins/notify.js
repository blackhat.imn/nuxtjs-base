class Notify {
  static success(handler, message) {
    handler.toast(message, {
      title: 'DSVN',
      toaster: 'b-toaster-top-center',
      solid: true,
      variant: 'success'
    })
  }

  static warning(handler, message) {
    handler.toast(message, {
      title: 'DSVN',
      toaster: 'b-toaster-top-center',
      solid: true,
      variant: 'warning'
    })
  }

  static error(handler, message) {
    handler.toast(message, {
      title: 'DSVN',
      toaster: 'b-toaster-top-center',
      solid: true,
      variant: 'danger'
    })
  }
}

export default function(context, inject) {
  inject('notify', Notify)
}
