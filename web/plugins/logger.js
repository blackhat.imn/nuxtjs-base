export class Logger {
  static color = {
    debug: '#0db8de',
    info: '#32CD32',
    error: '#e52b50'
  }

  static debug(message) {
    if (process.env.DEBUG) {
      console.log(message)
    }
  }

  static info(message) {
    if (process.env.DEBUG) {
      console.log('%c ' + message, 'color: ' + Logger.color.info)
    }
  }

  static error(error) {
    if (process.env.DEBUG) {
      console.log('%c' + error, 'color: ' + Logger.color.error)
      console.log(error)
    }
  }

  static logRequest(config) {
    if (process.env.DEBUG) {
      console.log(`%cBEGIN REQUEST ${config.method.toUpperCase()}: ${config.url}`, 'color: ' + Logger.color.info)
      this.debug(config)
      console.log('%cEND REQUEST.', 'color: ' + Logger.color.info)
    }
  }

  static logResponseSuccess = (response) => {
    if (process.env.DEBUG) {
      console.log('%cGET RESPONSE SUCCESS', 'color: ' + Logger.color.info)
      this.debug(response)
      console.log('%cEND RESPONSE SUCCESS.', 'color: ' + Logger.color.info)
    }
  }

  static logResponseError = (error) => {
    if (process.env.DEBUG) {
      console.log('%cGET RESPONSE ERROR', 'color: ' + Logger.color.error)
      this.error(error)
      console.log('%cEND RESPONSE ERROR.', 'color: ' + Logger.color.error)
    }
  }
}

export default (context,inject) => {
  inject('logger', Logger)
}