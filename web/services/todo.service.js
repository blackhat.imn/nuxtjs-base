import todoEndpoint from '@/endpoints/todo'

export default class TodoService {
  constructor(api) {
    this.api = api
  }

  get() {
    return this.api.get(todoEndpoint.index).then(response => {
      return response.data.todos
    })
  }
}