import {
  HTTP_STATUS
} from '@/config/consts'

import authenticationEndpoint from '@/endpoints/auth'

export default class AuthService {
  constructor(api) {
    this.api = api
  }

  login(credential) {
    return this.api.post(authenticationEndpoint.login, credential).then(response => {
      if (response.status == HTTP_STATUS.SUCCESS) {
        let user = response.data.loggedInUserInfo;

        if (user) {
          window.localStorage.setItem('user', JSON.stringify(user));
          return true;
        }
      }

      return false
    })
  }
}

