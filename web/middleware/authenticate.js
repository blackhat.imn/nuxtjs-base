export default async function ({ app, redirect }) {
  let loginState = await app.$cookies.get('loggedInState')

  if (loginState != 'logged_in' && loginState != 'remember_logged_in')
    redirect('/auth/login')
}